import { observable, computed, action } from 'mobx'

export default class PermilStore {
  @observable permil = 0

  @computed get permilToAngle() {
    return Math.atan(this.permil / 1000) / Math.PI * 180
  }
}
