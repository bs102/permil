import React from 'react'
import ReactDOM from 'react-dom'
import { observer } from 'mobx-react'
import { action } from 'mobx'

@observer
export default class PermilInput extends React.Component {
  render() {
    return (
      <div className="permilInputContainer">
        <div>
          <input
            type="text"
            ref="newPermil"
            className="permil"
            placeholder={this.props.permilStore.permil}
            onChange={this.handleOnChange}
          />
        </div>
        <div>
          <p>‰</p>
        </div>
      </div>
    )
  }

  @action handleOnChange = (event) => {
    let np = this.refs.newPermil.value
    if (np.match(/^\d+(?:\.\d+)?$/)) {
      this.props.permilStore.permil = np
    }else if (np == '') {
      this.props.permilStore.permil = 0
    }
  }
}
