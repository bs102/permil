import React from 'react'
import PermilInput from './PermilInput'
import PermilArea from './PermilArea'

export default class App extends React.Component {
  render() {
    return (
      <div>
        <PermilInput {...this.props} />
        <PermilArea {...this.props} />
      </div>
    )
  }
}
