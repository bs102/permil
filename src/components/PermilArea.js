import React from 'react'
import { observer } from 'mobx-react'

@observer
export default class PermilArea extends React.Component {
  render() {
    return (
      <div className="permilAreaContainer">
        <div
          className="permilArea"
          style={{ transform: 'rotate(' + this.props.permilStore.permilToAngle + 'deg)' }}
        ></div>
      </div>
    )
  }
}
