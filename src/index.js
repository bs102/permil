import React from 'react'
import ReactDOM from 'react-dom'
import PermilStore from './stores/PermilStore'
import App from './components/App'
import 'ress'
import './stylesheets/style.css'

var permilStore = new PermilStore()

ReactDOM.render(<App permilStore={permilStore} />, document.querySelector('#root'))
